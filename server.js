const express = require("express");
const app = express();

app.use(express.static("public"));

app.get("/animation-logo", (request, response) => {
  response.sendFile(__dirname + "/views/index.html");
});

app.get("/", (request, response) => {
  response.sendFile(__dirname + "/index.html");
});

app.get("/confluence-logo", (request, response) => {
  response.sendFile(__dirname + "/logo/index.html");

});


const listener = app.listen(process.env.PORT, () => {
  console.log("Your app is listening on port " + listener.address().port);
});
