(function () {

    const defaultText = `Jived fox
nymph grabs
quick waltz`;

    const fonts = {
        "loz": {
            label: "Lozenge",
            styles: ["", "1", "2"],
            customSpace: true,
            capped: true,
            categories: ["decor", "sans"]
        },
        "frnds": {
            label: "Friends",
            styles: [],
            inserter: 3,
            categories: ["decor"]
        },
        "honks": {
            label: "Honk",
            styles: [],
            customSpace: true,
            categories: ["decor"]
        },
        "poop": {
            label: "Poop",
            styles: [],
            categories: ["decor"]
        },
        "bounce": {
            label: "Bounce",
            animated: true,
            styles: [],
            categories: ["animated", "sans"]
        },
        "stsf": {
            label: "Serif",
            styles: ["rainbow", "0", "1", "2", "3", "4", "5", "6", "7", "8"],
            displayedStyle: "rainbow",
            categories: ["serif"]
        },
        "supreme": {
            label: "Supreme",
            styles: [],
            customSpace: true,
            categories: ["sans"]
        },
        "calc": {
            label: "Calculator",
            styles: [],
            categories: ["decor"]
        },
        "glitter": {
            label: "Glitter",
            animated: true,
            styles: [],
            categories: ["animated", "sans"]
        },
        "8ies": {
            label: "Eighties",
            styles: [],
            categories: ["decor", "sans"]
        },
        "comicsans": {
            label: "Comic Sans",
            styles: [],
            categories: ["sans"]
        },
        "fest": {
            label: "Festive",
            styles: [],
            categories: ["decor", "sans"]
        },
        "bones": {
            label: "Bones",
            styles: [],
            categories: ["decor"]
        },
        "caution": {
            label: "Caution Tape",
            styles: [],
            customSpace: true,
            categories: ["decor", "sans"]
        },
        "eh": {
            label: "Enhance",
            styles: ["x", "2x"],
            displayedStyle: "x",
            categories: ["decor", "sans"]
        },
        "uwu": {
            label: "uwu",
            styles: [],
            categories: ["decor", "sans"]
        },
        "fancy": {
            label: "Fancy",
            styles: [],
            categories: ["decor", "serif"]
        },
        "fire": {
            label: "Fire",
            animated: true,
            styles: [],
            customSpace: true,
            categories: ["animated", "sans"]
        },
        "gaudy": {
            label: "Gaudy",
            styles: [],
            categories: ["decor", "serif"]
        },
        "horror": {
            label: "Horror",
            styles: [],
            categories: ["decor"]
        },
        "jelly": {
            label: "Jelly",
            animated: true,
            styles: [],
            categories: ["animated", "sans"]
        },
        "kanjq": {
            label: "Kanji-esque",
            styles: [],
            categories: ["decor"]
        },
        "metal": {
            label: "Metal",
            styles: [],
            categories: ["decor", "sans"]
        },
        "neon": {
            label: "Neon",
            styles: [],
            categories: ["decor", "sans"]
        },
        "retro": {
            label: "Retro",
            styles: [],
            categories: ["serif"]
        },
        "rusprop": {
            label: "Propaganda",
            styles: [],
            categories: ["decor"]
        },
        "snow": {
            label: "Snow",
            animated: true,
            styles: [],
            categories: ["animated", "sans"]
        },
        "business": {
            label: "Business",
            styles: [],
            categories: ["serif"]
        },
        "ztoa": {
            label: "Sans",
            styles: ["rainbow", "0", "1", "2", "3", "4", "5", "6", "7", "8"],
            displayedStyle: 5,
            categories: ["sans"]
        }
    };

    const keyChart = {
        "_a": "lower-a",
        "_b": "lower-b",
        "_c": "lower-c",
        "_d": "lower-d",
        "_e": "lower-e",
        "_f": "lower-f",
        "_g": "lower-g",
        "_h": "lower-h",
        "_i": "lower-i",
        "_j": "lower-j",
        "_k": "lower-k",
        "_l": "lower-l",
        "_m": "lower-m",
        "_n": "lower-n",
        "_o": "lower-o",
        "_p": "lower-p",
        "_q": "lower-q",
        "_r": "lower-r",
        "_s": "lower-s",
        "_t": "lower-t",
        "_u": "lower-u",
        "_v": "lower-v",
        "_w": "lower-w",
        "_x": "lower-x",
        "_y": "lower-y",
        "_z": "lower-z",
        "_á": "lower-a-acute",
        "_é": "lower-e-acute",
        "_í": "lower-i-acute",
        "_ó": "lower-o-acute",
        "_ú": "lower-u-acute",
        "_ğ": "lower-g-breve",
        "_ç": "lower-c-cedilla",
        "_ş": "lower-s-cedilla",
        "_â": "lower-a-circumflex",
        "_ê": "lower-e-circumflex",
        "_î": "lower-i-circumflex",
        "_ô": "lower-o-circumflex",
        "_û": "lower-u-circumflex",
        "_ı": "lower-i-dotless",
        "_à": "lower-a-grave",
        "_è": "lower-e-grave",
        "_ì": "lower-i-grave",
        "_ò": "lower-o-grave",
        "_ù": "lower-u-grave",
        "_å": "lower-a-ring",
        "_ñ": "lower-n-tilde",
        "_ä": "lower-a-umlaut",
        "_ë": "lower-e-umlaut",
        "_ï": "lower-i-umlaut",
        "_ö": "lower-o-umlaut",
        "_ü": "lower-u-umlaut",
        "_ÿ": "lower-y-umlaut",
        "_0": "0",
        "_1": "1",
        "_2": "2",
        "_3": "3",
        "_4": "4",
        "_5": "5",
        "_6": "6",
        "_7": "7",
        "_8": "8",
        "_9": "9",
        "_A": "a",
        "_B": "b",
        "_C": "c",
        "_D": "d",
        "_E": "e",
        "_F": "f",
        "_G": "g",
        "_H": "h",
        "_I": "i",
        "_J": "j",
        "_K": "k",
        "_L": "l",
        "_M": "m",
        "_N": "n",
        "_O": "o",
        "_P": "p",
        "_Q": "q",
        "_R": "r",
        "_S": "s",
        "_T": "t",
        "_U": "u",
        "_V": "v",
        "_W": "w",
        "_X": "x",
        "_Y": "y",
        "_Z": "z",
        "_Z": "z",
        "_Á": "a-acute",
        "_É": "e-acute",
        "_Í": "i-acute",
        "_Ó": "u-acute",
        "_Ú": "u-acute",
        "_Ğ": "g-breve",
        "_Ç": "c-cedilla",
        "_Ş": "s-cedilla",
        "_Â": "a-circumflex",
        "_Ê": "e-circumflex",
        "_Î": "i-circumflex",
        "_Ô": "u-circumflex",
        "_Û": "u-circumflex",
        "_İ": "i-dotted",
        "_À": "a-grave",
        "_È": "e-grave",
        "_Ì": "i-grave",
        "_Ò": "u-grave",
        "_Ù": "u-grave",
        "_Å": "a-ring",
        "_Ñ": "n-tilde",
        "_Ä": "a-umlaut",
        "_Ë": "e-umlaut",
        "_Ï": "i-umlaut",
        "_Ö": "u-umlaut",
        "_Ü": "u-umlaut",
        "_Ÿ": "y-umlaut",
        "_!": "_exclamation",
        "_?": "_question",
        "_!": "_exclamation",
        "_%": "_percent",
        "_&": "_ampersand",
        "_*": "_asterisk",
        "_@": "_at",
        "_^": "_carat",
        "_:": "_colon",
        "_.": "_period",
        "_;": "_semicolon",
        "_,": "_comma",
        "_+": "_plus",
        "_-": "_minus",
        "_=": "_equal",
        "_>": "_greaterthan",
        "_<": "_lessthan",
        "_#": "_hash",
        "_[": "_leftbracket",
        "_]": "_rightbracket",
        "_{": "_leftcurly",
        "_}": "_rightcurly",
        "_“": "_leftdquote",
        "_”": "_rightdquote",
        "_‘": "_leftsquote",
        "_’": "_rightsquote",
        "_`": "_backtick",
        "_\\": "_backslash",
        "_/": "_forwardslash",
        "_'": "_rightsquote",
        "_\"": "_rightdquote",
        "_(": "_leftparen",
        "_)": "_rightparen",
        "__": "_underscore",
        "_~": "_tilde",
        "_|": "_vertical",
        "_$": "_dollar"
    };
    const textinput = document.getElementById("textinput");
    const searchinput = document.getElementById("searchinput");
    const clearsearch = document.getElementById("clearsearch");
    const categories = document.getElementsByName("category");
    const examples = document.getElementById("examples");

    function conversion() {
        const rainbowMaxColors = 9;
        let category;
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].checked) {
                category = categories[i].value;
                break;
            }
        }
        let fontsFound = 0;
        examples.innerHTML = "";

        for (let font in fonts) {

            let searchVal = searchinput.value.toLowerCase();
            let fontSearchString = (font + " " + fonts[font].label).toLowerCase();
            let isInSearchField = fontSearchString.indexOf(searchVal) > -1 || searchVal == "";
            let isInFontCategories = fonts[font].categories.indexOf(category) > -1 || category == "all";

            if (isInSearchField && isInFontCategories) {
                fontsFound++;
                let typeFace = font;
                let rainbowSwitch = 0;
                let replaced = "";
                let displayed = "";

                let imageSuffix = fonts[font].animated ? "gif" : "png";

                if (fonts[font].displayedStyle) {
                    typeFace = font + fonts[font].displayedStyle;
                }

                let space = fonts[font].customSpace ? `${typeFace}-_space` : `space`;
                let texts = textinput.value.length ? textinput.value : fonts[font].label;

                let inserterInt = 0;
                for (let i = 0; i < texts.length; i++) {

                    let prevChar = i == 0 ? ' ' : texts.charAt(i - 1);
                    let inserterHTML = '';
                    let replacerText = '';
                    if (fonts[font].displayedStyle == "rainbow") {
                        typeFace = font + rainbowSwitch;
                    }
                    let char = texts.charAt(i);
                    let convertedChar = keyChart["_" + char];
                    
                    if (fonts[font].inserter && prevChar !== ' ' && prevChar !== "\n" && prevChar !== "\r") {
                        inserterInt = inserterInt < fonts[font].inserter ? inserterInt + 1 : 1;
                        inserterHTML = `<img src="letters/${typeFace}/${typeFace}-ins${inserterInt}.${imageSuffix}">`;
                        replacerText = `:${typeFace}-ins${inserterInt}:`;
                    }
                    if (i == 0 && fonts[font].capped) {
                        replaced += `:${typeFace}-cap-pre:`;
                        displayed += `<img src="letters/${typeFace}/${typeFace}-cap-pre.${imageSuffix}">`;
                    }
                    if (convertedChar) {
                        replaced += replacerText;
                        replaced += `:${typeFace}-${convertedChar}:`;
                        if (i < 48) {
                            displayed += inserterHTML;
                            displayed += `<img src="letters/${typeFace}/${typeFace}-${convertedChar}.${imageSuffix}">`;
                        }
                    } else if (char == " ") {
                        rainbowSwitch--;
                        replaced += `:${space}:`;
                        if (i < 48) {
                            displayed += `<img src="letters/${fonts[font].customSpace ? `${typeFace}/`: ``}${space}.png">`;
                        }
                    } else if (char == "\n" || char == "\r") {
                        replaced += char;
                        displayed += `<br>`;
                    } else {
                        replaced += char;
                        replaced += char;
                    }

                    rainbowSwitch++;
                    if (rainbowSwitch == rainbowMaxColors) {
                        rainbowSwitch = 0;
                    }

                    if (i == texts.length - 1 && fonts[font].capped) {
                        replaced += `:${typeFace}-cap-end:`;
                        displayed += `<img src="letters/${typeFace}/${typeFace}-cap-end.${imageSuffix}">`;
                    }
                }

                let previewFont = document.createElement("div");
                previewFont.className = "font-example";

                let previewHeader = document.createElement("div");
                previewHeader.className = "header";

                let textSelect = document.createElement("textarea");
                textSelect.innerHTML = replaced;

                let copyText = document.createElement("button");
                copyText.innerHTML = "&nbsp;Copy&nbsp;";
                copyText.onclick = function (e) {
                    if (textSelect.value.length) {
                        textSelect.select();
                        document.execCommand("copy");
                        e.target.style.backgroundColor = "green";
                        e.target.innerHTML = "Copied";
                        setTimeout(() => {
                            textSelect.blur();
                            e.target.style = "";
                            e.target.innerHTML = "&nbsp;Copy&nbsp;";
                        }, 2000);
                    }
                }

                let previewImgs = document.createElement("div");
                previewImgs.className = "images";
                previewImgs.innerHTML += displayed;

                let fontLabel = document.createElement("label");
                fontLabel.innerHTML = fonts[font].label;


                if (fonts[font].styles.length > 0) {
                    let selector = document.createElement("div");
                    selector.className = "select-dropdown";
                    for (let s = 0; s < fonts[font].styles.length; s++) {
                        let fStyle = fonts[font].styles[s];
                        let selectButton = document.createElement("button");
                        selectButton.onclick = function () {
                            if (fonts[font].displayedStyle !== fStyle) {
                                fonts[font].displayedStyle = fStyle;
                                conversion();
                            }
                        };
                        if (fStyle == fonts[font].displayedStyle) {
                            selectButton.style.order = 0;
                        }
                        let iR = fonts[font].styles[s] == "rainbow";
                        selectButton.innerHTML = `${fonts[font].label}<img src="letters/space.png"><img src="letters/${font}${iR ? 1 : fStyle}/${font}${iR ? 1 : fStyle}-s.${imageSuffix}"><img src="letters/${font}${iR ? 2 : fStyle}/${font}${iR ? 2 : fStyle}-t.${imageSuffix}"><img src="letters/${font}${iR ? 3 : fStyle}/${font}${iR ? 3 : fStyle}-y.${imageSuffix}"><img src="letters/${font}${iR ? 4 : fStyle}/${font}${iR ? 4 : fStyle}-l.${imageSuffix}"><img src="letters/${font}${iR ? 5 : fStyle}/${font}${iR ? 5 : fStyle}-e.${imageSuffix}"><img src="letters/space.png">${fStyle == "rainbow" ? `<img src="letters/rainbow.png">` : fStyle}`;
                        selector.appendChild(selectButton);

                    }
                    previewHeader.appendChild(selector);

                } else {
                    previewHeader.appendChild(fontLabel);
                }

                previewHeader.appendChild(copyText);
                previewFont.appendChild(previewHeader);
                let overflowArea = document.createElement("div");
                overflowArea.className = "no-overflow";
                overflowArea.appendChild(textSelect);
                overflowArea.appendChild(previewImgs);

                previewFont.appendChild(overflowArea)
                examples.appendChild(previewFont);
            }
        }
        if (fontsFound == 0) {
            examples.innerHTML = `<h2>o no where'd all the fonts go</h2>`;
        }
    }

    function offsetTopFind(el, withHeight = 0) {
        let yPosition = withHeight;
        while (el.offsetParent !== null) {
            yPosition += el.offsetTop;
            el = el.offsetParent;
        }
        return yPosition;
    }

    function scrollLoadImg() {
        let winTop = window.scrollY;
        let winBot = window.innerHeight;
        let winVis = winTop + winBot;
        let unLoaded = document.querySelectorAll("[data-lz]");

        if (unLoaded.length) {
            for (let l = 0; l < unLoaded.length; l++) {
                let itemTop = offsetTopFind(unLoaded[l], 0, l == 0);
                let itemHeight = itemTop + unLoaded[l].offsetHeight;
                if (itemTop < winVis && itemHeight > winTop) {
                    let src = unLoaded[l].getAttribute("data-lz");
                    unLoaded[l].removeAttribute("data-lz");
                    unLoaded[l].src = src;
                }
            }
        }
    }
    categories.forEach((item) => {
        item.addEventListener("click", () => {
            conversion();
        });
    })
    searchinput.addEventListener("keyup", () => {
        conversion();
    });
    textinput.addEventListener("keyup", () => {
        conversion();
    });
    clearsearch.addEventListener("click", () => {
        searchinput.value = "";
        conversion();
    });

    conversion();

})();